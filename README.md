Graphs and Things
=================

To compile:
 Defaults to using clang as a compiler. Run with "make CC=gcc" to compile with gcc

To run:
 ./main <test case file>

Graphs are defined by the following syntax:

node -> node

where 'node' is an integer.
This tells the program that there is a *directed* edge leading from the first number to the second.
Thus, the following is an example of a cyclic directed graph, where node n points to node (n+1 (mod 3)) + 1.

1 -> 2

2 -> 3

3 -> 1


Undirected graphs can be simulated by providing the inverse connection. For example, the following graph is a bidirection connection between three nodes.

1 -> 2

2 -> 1

2 -> 3

3 -> 2

3 -> 1

1 -> 3

Interpreting the Output
=======================

Currently, the main testing program (from main.c) interprets the lovely inputted test case, and creates an adjacency list and matrix.
It then displays the matrix, which can be written to a file by either redirecting stdout (the lazy way), or by calling the function fprint\_matrix(), which takes a FILE\* as the second argument.

It then runs a BFS and DFS search, and prints out the tree that is generated. Note: this is in an adjacency matrix form, thus must be interpreted as such.

A topological sort is run, and prints out the sorted list upon completion.

Finally, the adjacency matrix is converted to a new adjacency list, and the old list is converted to a new matrix. Both are then printed out, which should be identical the originally printed lists at the beginning of execution. Note: the nodes/sides/whatevers may not be in the same order, as the conversion method does NOT preserve what order the nodes were originally read in.

This program leaks a ton of memory, and is a known problem.