CC=clang
CFLAGS=-Wall

all: main

main: main.c graphs.o linkedlist.o search.o
	${CC} ${CFLAGS} -g $^ -o $@

graphs.o: graphs.c graphs.h
	${CC} ${CFLAGS} -g -c $<

linkedlist.o: linkedlist.c linkedlist.h
	${CC} ${CFLAGS} -g -c $<

search.o: search.c search.h
	${CC} ${CFLAGS} -g -c $<

clean:
	rm -rf *.o main
