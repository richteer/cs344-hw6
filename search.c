#include <stdio.h>
#include <string.h>
#include "search.h"
#include "linkedlist.h"


static int handle_list(node_t ** queue, adjmat_t * am, adjmat_t * tree, node_t ** seen, int (*addlist)(node_t**,int))
{
	int value;
	int i, ind;

	if (NULL == *queue) {
		return -1;
	}

	value = ll_popfront(queue);

	for (i = 0; i < am->num_values; i++) {
		if (am->value_list[i] == value) break;
	}
	ind = i;

	ll_append(seen, value);
	for (i = 0; i < am->num_values; i++) {
		if ((am->matrix[ind][i]) && (NULL == ll_find(*seen,am->value_list[i]))) {
			addlist(queue,am->value_list[i]);
			tree->matrix[ind][i] = 1;
			ll_append(seen,am->value_list[i]);
			printf("-> %d\n",am->value_list[i]);
		}
	}

	return 0;
}


// Assume tree is preallocated
static int search_matrix(adjmat_t *am, adjmat_t *tree, int (*addlist)(node_t**,int))
{
	int i;
	node_t * queue = NULL;
	node_t * seen = NULL;

	tree->num_values = am->num_values;
	tree->value_list = malloc(sizeof(int) * am->num_values);
	memcpy(tree->value_list, am->value_list, sizeof(int) * am->num_values);

	tree->matrix = malloc(sizeof(char*)*am->num_values);
	for (i = 0; i < am->num_values; i++)
		tree->matrix[i] = calloc(1,sizeof(char)*am->num_values);

	ll_append(&queue,am->value_list[0]); // Change the index to pick a different starting value
	while (!handle_list(&queue, am, tree,  &seen, addlist)); // Handle the queue until it is empty

	ll_free(seen);
	print_matrix(tree);

	return 0;
}


int bfs_matrix(adjmat_t * am, adjmat_t * tree)
{
	return search_matrix(am,tree,ll_append);
}
int dfs_matrix(adjmat_t * am, adjmat_t * tree)
{
	return search_matrix(am,tree,ll_prepend);
}


static int topo_visit_matrix(adjmat_t * am, int index, node_t ** seen, node_t ** hold, node_t ** list)
{
	int i;

	// TODO: Maybe NULL check the pointers here

	if (NULL != ll_find(*hold, am->value_list[index])) {
		printf("***cycle detected on %d***\n",am->value_list[index]);
		return -1;
	}
	else if (NULL != ll_find(*seen, am->value_list[index])) {
		return 0;
	}

	ll_prepend(hold, am->value_list[index]);

	for (i = 0; i < am->num_values; i++) {
		if (am->matrix[index][i])
			topo_visit_matrix(am,i,seen,hold,list);
	}

	ll_popfront(hold);
	ll_prepend(seen, am->value_list[index]);
	ll_prepend(list, am->value_list[index]);
	

	return 0;
}


int topo_matrix(adjmat_t * am, node_t ** list)
{
	int i;
	int ret = 0;
	node_t * seen = NULL;
	node_t * hold = NULL;

	if (NULL == *list) {
		ll_free(*list);
		*list = NULL;
	}

	for (i = 0; i < am->num_values; i++) {
		
		if (NULL != ll_find(seen, am->value_list[i])) continue;

		if (-1 == topo_visit_matrix(am, i, &seen, &hold, list)) {
			fprintf(stderr,"Cycle detected\n");
			ret = -1;
			ll_free(*list);
			goto cleanup;
		}
	}


cleanup:
	
	ll_free(seen);
	ll_free(hold);
	return ret;	
}
