#include <stdio.h>
#include <stdlib.h>
#include "graphs.h"

static int ctoi(int * value_list, int size, int value)
{
	int i;

	for (i = 0; i < size; i++) {
		if (value_list[i] == value) return i;
	}

	return -1;
}


int readfile(const char * filename, connect_t ** cts, size_t * cts_size)
{
	int size = 8;
	FILE * file;

	file = fopen(filename,"r");
	
	*cts = malloc(sizeof(connect_t)*size);
	*cts_size = 0;

	while (!feof(file)) {
		if (*cts_size > size) {
			*cts = realloc(*cts,sizeof(connect_t) * (size = size << 1));
			if (NULL == cts) return -2;
			printf("reallocating\n");
		}
		if (-1 == fscanf(file,"%d -> %d", &(*cts)[*cts_size].node, &(*cts)[*cts_size].to)) break;
		(*cts_size)++;
	}
	
	fclose(file);

	return 0;
}

int create_adjacency_list(const connect_t * cts, const size_t cts_size, adjlist_t * adls)
{
	int i, j;
	adls->num_values = 0;
	node_t * temp;
	node_t * head = NULL;

	// Find all uniques
	for (i = 0; i < cts_size; i++) {
		if (NULL == ll_find(head, cts[i].node)) {
			ll_append(&head, cts[i].node);
		}
		if (NULL == ll_find(head, cts[i].to)) {
			ll_append(&head, cts[i].to);
		}
	}

	//Store number of uniques
	adls->num_values = ll_length(head);
	adls->list = calloc(1, sizeof(node_t *) * adls->num_values);
	temp = head;

	//Put all uniques into the heads of the adjacency list
	for (i = 0; i < adls->num_values; i++) {
		ll_append(adls->list + i, temp->value);
		temp = temp->next;
	}

	//Put the adjacency in adjacency list
	//TODO check if element to add is a repeat
	for (i = 0; i < adls->num_values; i++) {
		for (j = 0; j < cts_size; j++) {
			if (adls->list[i]->value == cts[j].node) {
				ll_append((adls->list)+i, cts[j].to);
			}
		}
	}
	
	ll_free(head);
	return 0;
}


int create_adjacency_matrix(const connect_t * cts, const size_t cts_size, adjmat_t * mt)
{
	int i;
	mt->num_values = 0;
	node_t * head = NULL;

	//Get all unique node values and put them in a linked list starting at head
	for (i = 0; i < cts_size; i++) {
		if (NULL == ll_find(head, cts[i].node)) {
			ll_append(&head, cts[i].node);
		}
		if (NULL == ll_find(head, cts[i].to)) {
			ll_append(&head, cts[i].to);
		}
	}
	
	//num_values now holds the number of unique nodes
	mt->num_values = ll_length(head);
	mt->value_list = malloc(sizeof(int)*mt->num_values);	
	node_t * next = head;
	//Put all unique values into the adjacency matrix
	for (i = 0; i < mt->num_values; i++) {
		mt->value_list[i] = next->value;
		next = next->next;
	}	

	//malloc the space for the two-dimensional array to hold the adjacency matrix
	mt->matrix = malloc(sizeof(char *) * mt->num_values);
	for (i = 0; i < mt->num_values; i++) {
		mt->matrix[i] = calloc(1,sizeof(char) * mt->num_values);
	}

	for (i = 0; i < cts_size; i++) {
		mt->matrix[ctoi(mt->value_list,mt->num_values,cts[i].node)][ctoi(mt->value_list,mt->num_values,cts[i].to)] = 1;
	}


	ll_free(head);
	return 0;
}


int list_to_matrix(adjlist_t * al, adjmat_t * mt)
{
	int i, size, head, cts_size;
	size = 0;
	cts_size = 1;
	node_t * temp = *(al->list);
	connect_t * cts = malloc(sizeof(connect_t));

	//Step through each linked list and add the entry
	for (i = 0; i < al->num_values; i++) {
		head = (temp = al->list[i])->value;
		//Make sure I'm not adding a self-connection
		while (temp != NULL) {
			// TODO: Fix for self-connections other than the first element of the linked list
			if (head != temp->value) {
				if (size > cts_size) cts = realloc(cts, sizeof(connect_t) * (cts_size = cts_size << 1));
				cts[size].node = head;
				cts[size].to = temp->value;
				size++;
			}
			temp = temp->next;
		}
	}
	create_adjacency_matrix(cts, size, mt);
	return 0;
}


int matrix_to_list(adjmat_t * mt, adjlist_t * al)
{
	int i, j, size, cts_size;
	size = 0;
	cts_size = 1;
	connect_t * cts = malloc(sizeof(connect_t));
	
	//Run through each index of the array, add an entry if needed
	for (i = 0; i < mt->num_values; i++) {
		for (j = 0; j < mt->num_values; j++) {
			if (mt->matrix[i][j]) {
				cts[size].node = mt->value_list[i];
				cts[size].to = mt->value_list[j];
				size++;
				if (size > cts_size) cts = realloc(cts, sizeof(connect_t) * (cts_size = cts_size << 1));
			}
		}
	}
	
	create_adjacency_list(cts, size, al);
	return 0;
}


void fprint_matrix(adjmat_t * mt, FILE * file)
{
	int i, j;
	fprintf(file, "    ");
	for (i = 0; i < mt->num_values; i++)
		fprintf(file, "%d ",mt->value_list[i]);

	fprintf(file, "\n  +-");

	for (i = 0; i < mt->num_values; i++)
		fprintf(file, "--");
	fprintf(file, "\n");
	for (i = 0; i < mt->num_values; i++) {
		fprintf(file, "%2d| ",mt->value_list[i]);

		for (j = 0; j < mt->num_values; j++) {
			fprintf(file, "%c ", (mt->matrix[i][j])?'1':'0');
		}
		fprintf(file, "\n");
	} 


}


void fprint_list(adjlist_t * al, FILE * file)
{
	int i;

	for (i = 0; i < al->num_values; i++) {
		ll_fprint(al->list[i],file);
	}

}


void print_matrix(adjmat_t * mt)
{
	fprint_matrix(mt, stdout);
}

void print_list(adjlist_t *al)
{
	fprint_list(al, stdout);
}
