#ifndef _graphs_h_
#define _graphs_h_

#include <stdlib.h>
#include "linkedlist.h"

typedef struct {
	int node;
	int to;
} connect_t;

typedef struct {
	node_t ** list;
	int num_values;
} adjlist_t;

typedef struct {
	char ** matrix;
	int * value_list;
	int num_values;
} adjmat_t;

int readfile(const char * filename, connect_t ** cts, size_t * cts_size);
int create_adjacency_list(const connect_t * cts, const size_t cts_size, adjlist_t * adls);
int create_adjacency_matrix(const connect_t * cts, const size_t cts_size, adjmat_t * mt);
int list_to_matrix(adjlist_t * al, adjmat_t * mt);
int matrix_to_list(adjmat_t * mt, adjlist_t * al);
void fprint_matrix(adjmat_t * mt, FILE * file);
void fprint_list(adjlist_t * al, FILE * file);
void print_matrix(adjmat_t * mt);
void print_list(adjlist_t * al);
#endif
