#ifndef _linkedlist_h_
#define _linkedlist_h_
#include <stdio.h>


typedef struct node_s {
	int value;
	struct node_s * next;
} node_t;

node_t * ll_find(node_t * head, int value);
int ll_append(node_t ** head, int value);
int ll_prepend(node_t ** head, int value);
int ll_popfront(node_t ** head);
int ll_length(node_t * head);
int ll_free(node_t * head);
void ll_fprint(node_t * head, FILE * file);
void ll_print(node_t * head);
#endif
