#ifndef _search_h_
#define _search_h_

#include "graphs.h"

int bfs_matrix(adjmat_t *am, adjmat_t * tree);
//tint bfs_list(adjlist_t *al);
int dfs_matrix(adjmat_t * am, adjmat_t * tree);
// int dfs_list(adjlist_t *al);
int topo_matrix(adjmat_t * am, node_t ** list);

#endif
