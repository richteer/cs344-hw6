#include <stdio.h>
#include "graphs.h"
#include "search.h"
#include "linkedlist.h"

int main(int argc, char ** argv)
{
	connect_t *cts;
	adjlist_t al, al2;
	adjmat_t am, am2;
	size_t cts_size;
	node_t * list = NULL;
	adjmat_t tree;

	if (argc < 2) {
		fprintf(stderr,"Usage: %s <graph description filename>\n",argv[0]);
		return 1;
	}

	readfile(argv[1], &cts, &cts_size); // TEMPORARY

	create_adjacency_matrix(cts,cts_size,&am);
	create_adjacency_list(cts,cts_size,&al);

	print_matrix(&am);
	printf("\n");
	print_list(&al);

	printf("\nBFS:\n");
	bfs_matrix(&am, &tree);
	printf("\nDFS:\n");
	dfs_matrix(&am, &tree);

	printf("\nPerforming toposort\n");
	topo_matrix(&am, &list);
	ll_print(list);

	printf("\nConverting list->matrix and matrix->list\n");
	list_to_matrix(&al, &am2);
	matrix_to_list(&am, &al2);
	
	print_matrix(&am2);
	print_list(&al2);

	free(cts);
}
