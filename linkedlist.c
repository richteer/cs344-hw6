#include "graphs.h"
#include "linkedlist.h"

void ll_fprint(node_t * head, FILE * file)
{
	node_t * cur;

	if (NULL == head) goto done;

	for (cur = head; cur != NULL; cur = cur->next) {
		fprintf(file, "%d->",cur->value);
	} 

done:
	fprintf(file, "NULL\n");
}


void ll_print(node_t * head)
{
	ll_fprint(head, stdout);
}


node_t * ll_find(node_t * head, int value)
{
	node_t * cur;
	if (NULL == head) return NULL;

	for (cur = head; cur != NULL; cur = cur->next) {
		if (cur->value == value) return cur;
	}
	// Failure...
	return NULL;
}


int ll_append(node_t ** head, int value)
{
	node_t * cur;

	if (NULL == *head) {
		*head = malloc(sizeof(node_t));
		(*head)->value = value;
		(*head)->next = NULL;
		return 0;
	}
		
	for (cur = *head; cur->next != NULL; cur = cur->next) {
		// Wait for it...
	}

	cur->next = malloc(sizeof(node_t));
	cur->next->value = value;
	cur->next->next = NULL;

	return 0;

}


int ll_prepend(node_t ** head, int value)
{
	node_t * nt;	

	if (NULL == *head) {
		*head = malloc(sizeof(node_t));
		(*head)->value = value;
		(*head)->next = NULL;
		return 0;
	}
	
	nt = (*head);
	*head = malloc(sizeof(node_t));
	(*head)->value = value;
	(*head)->next = nt;
	
	return 0;
}


int ll_popfront(node_t ** head)
{
	node_t * temp;
	int value;

	if (NULL == *head) return -1;

	temp = *head;
	*head = (*head)->next;

	value = temp->value;
	free(temp);

	return value;
}


int ll_length(node_t *head)
{
	node_t * cur;
	int i = 0;
	if (NULL == head) return 0;

	for (cur = head; cur != NULL; cur = cur->next) {
		i++;
	}

	return i;
}


int ll_free(node_t * head)
{
	node_t * cur;
	node_t * prev;

	if (NULL == head) return -1;

	cur = head;
	while (1) {
		if (NULL == cur) break;
		prev = cur = cur->next;
		free(prev);
	}

	free(cur);
	return 0;
}
